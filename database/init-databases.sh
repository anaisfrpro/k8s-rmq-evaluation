#!/bin/sh
# Here add initialisation code for your DB

#!/bin/sh
set -e

# Récupération variables d'environnement
if [ -f "backend/.env" ]; then
    echo "Loading environment variables from backend/.env file..."
    export $(grep -v '^#' backend/.env | xargs)
else
    echo "Error: backend/.env file not found."
    exit 1
fi

# Création user PostgreSQL
psql -v ON_ERROR_STOP=1 --username "$PGUSER" --dbname "$PGDATABASE" <<-EOSQL
  CREATE USER $PGUSER WITH PASSWORD '$PGPASSWORD';
EOSQL

# Création database PostgreSQL
psql -v ON_ERROR_STOP=1 --username "$PGUSER" <<-EOSQL
  CREATE DATABASE $PGDATABASE;
EOSQL

# Privilège user sur la db
psql -v ON_ERROR_STOP=1 --username "$PGUSER" --dbname "$PGDATABASE" <<-EOSQL
  GRANT ALL PRIVILEGES ON DATABASE $PGDATABASE TO $PGUSER;
EOSQL
