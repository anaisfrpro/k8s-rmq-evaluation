# Rendu examen Anaïs FREJABUE 

## Prérequis :

J'ai récupéré le fichier kubeconfig.yml qui permet de récupérer les informations de connexions au cluster distant.

Il faut ensuite ajouter cette nouvelle variable environemment :

```SH
 $env:KUBECONFIG="C:\Users\Anais FREJABUE\OneDrive\Documents\4ème année - DEVOPS\Mai 2024\kubeconfig.yml”
```

Pour vérifier l'accès au cluster distant et m'assurer qu'il est fonctionnel, je lance cette commande pour récupérer les noeuds existants :

```SH
kubectl get nodes
```

Je créé ensuite un namespace pour ne pas rentrer en conflit avec les autres collaborateurs :

```SH
kubectl create namespace anais-frejabue
```
Voici le retour lorsque le namespace est créé : namespace/anais-frejabue created

```SH
kubectl get namespaces
```
![Résultat cmd get namespaces](image-2.png)

Voici le résultat des commandes ci-dessus :

![Résultats des commandes ](image-1.png)

### RabbitMQ

Voici la commande utilisée pour l'installer :

```SH
kubectl apply -f "https://github.com/rabbitmq/cluster-operator/releases/latest/download/cluster-operator.yml"
```

Voici le code du Cluster :

```SH
apiVersion: rabbitmq.com/v1beta1
kind: RabbitmqCluster
metadata:
  name: production-rabbitmqcluster
spec:
  replicas: 3
  resources:
    requests:
      cpu: 500m
      memory: 1Gi
    limits:
      cpu: 1
      memory: 2Gi
  rabbitmq:
    additionalConfig: |
      log.console.level = info
      channel_max = 1700
      default_user= guest
      default_pass = guest
      default_user_tags.administrator = true
  service:
    type: LoadBalancer
```

Je créé ensuite un fichier de déploiement : rabbitmq-deployment
```SH
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rabbitmq
  namespace: anais-frejabue
  labels:
    app: rabbitmq
spec:
  replicas: 1
  selector:
    matchLabels:
      app: rabbitmq
  template:
    metadata:
      labels:
        app: rabbitmq
    spec:
      containers:
      - name: rabbitmq
        image: rabbitmq:3-management
        ports:
        - containerPort: 5672
        - containerPort: 15672
---
apiVersion: v1
kind: Service
metadata:
  name: rabbitmq
  namespace: anais-frejabue
spec:
  ports:
  - port: 5672
    targetPort: 5672
    name: amqp
  - port: 15672
    targetPort: 15672
    name: management
  selector:
    app: rabbitmq
```

**PostgreSQL**

Voici les commandes utilisées pour construire l’image docker et l'envoyer sur mon registre (hub docker) :

```SH
docker build -t anais1411/database-postgres -f Dockerfile .

docker push anais1411/database-postgres

```

![Capture Docker Hub](image-3.png)

Voici la commande pour créer mon fichier de déploiement pour la base de données :

```SH
kubectl create --namespace=anais-frejabue -f anais-frejabue-database-deployment.yaml 
```

![Résultat get deployments](image-7.png)

![Résultat get pods](image-8.png)

J'ai également modifié le fichier d'initialisation :

```bash
#!/bin/sh
# Here add initialisation code for your DB

#!/bin/sh
set -e

# Récupération variables d'environnement
if [ -f "backend/.env" ]; then
    echo "Loading environment variables from backend/.env file..."
    export $(grep -v '^#' backend/.env | xargs)
else
    echo "Error: backend/.env file not found."
    exit 1
fi

# Création user PostgreSQL
psql -v ON_ERROR_STOP=1 --username "$PGUSER" --dbname "$PGDATABASE" <<-EOSQL
  CREATE USER $PGUSER WITH PASSWORD '$PGPASSWORD';
EOSQL

# Création database PostgreSQL
psql -v ON_ERROR_STOP=1 --username "$PGUSER" <<-EOSQL
  CREATE DATABASE $PGDATABASE;
EOSQL

# Privilège user sur la db
psql -v ON_ERROR_STOP=1 --username "$PGUSER" --dbname "$PGDATABASE" <<-EOSQL
  GRANT ALL PRIVILEGES ON DATABASE $PGDATABASE TO $PGUSER;
EOSQL

```

Suite à cette modification j'ai relancé les commandes suivantes :

```SH
docker build -t anais1411/database-postgres -f Dockerfile .

docker push anais1411/database-postgres

kubectl delete --namespace=anais-frejabue -f anais-frejabue-database-deployment.yaml 
```

Voici le résultat de la création du déploiement postgres et des logs de ce dernier :

![Résultat déploiement, get deployments, pods](image-5.png)

![Affichage connexion postgres](image-6.png)


**Serveur - Backend**
Pour effectuer le déploiement j'ai tout d'abord créé une nouvelle image à partir d'un nouveau fichier dockerfile :

```SH
FROM node:20-alpine
COPY . .
RUN yarn
EXPOSE 4040
CMD yarn start
```

J'ai ensuite créé un nouveau fichier de déploiement pour le backend. (anais-frejabue-backend-deployment.yaml)

Ce fichier contient des variables d'environnements définis comme ceci :

```SH
          env:
            - name: PGHOST
              value: 'anais-frejabue-database.anais-frejabue.svc.cluster.local'
            - name: PORT
              value: '4040'
            - name: PGUSER
              value: 'count'
            - name: PGPORT
              value: '5432'
            - name: PGPASSWORD
              value: 'count'
            - name: RABBITMQ_URL
              value: 'amqp://rabbitmq.anais-frejabue.svc.cluster.local:5672'
            - name: QUEUE
              value: 'count'
```
Il est important que les Urls soient formatées de la manière suivante : 
[nom-du-service].[namespace].svc.cluster.local

